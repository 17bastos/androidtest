package br.com.digio.androidtest.ui.home

import DigioService
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import br.com.digio.androidtest.model.DigioProducts
import com.nhaarman.mockitokotlin2.given
import com.nhaarman.mockitokotlin2.mock
import org.junit.Assert.assertEquals
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import retrofit2.mock.Calls

class HomeViewModelTest {

    private val digioService : DigioService = mock()
    private val homeViewModel: HomeViewModel = HomeViewModel(digioService = digioService)

    @get:Rule
    var ruleSuccess: TestRule = InstantTaskExecutorRule()
    @Test
    fun `Given a call to service When successful Then loading status Should be success`() {
        given(digioService.getProducts()).willReturn(Calls.response(mock()))
        homeViewModel.getProducts()
        assertEquals(homeViewModel.loadingStatus.value, LoadingStatusEnum.SUCCESS)
    }

    @get:Rule
    var ruleFail: TestRule = InstantTaskExecutorRule()
    @Test
    fun `Given a call to service When it fails Then loading status Should be success`() {
        given(digioService.getProducts()).willReturn(Calls.failure(mock()))
        homeViewModel.getProducts()
        assertEquals(homeViewModel.loadingStatus.value, LoadingStatusEnum.ERROR)
    }
}