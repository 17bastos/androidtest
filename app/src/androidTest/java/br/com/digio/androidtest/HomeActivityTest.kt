package br.com.digio.androidtest

import androidx.lifecycle.Lifecycle
import androidx.test.core.app.launchActivity
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.platform.app.InstrumentationRegistry
import br.com.digio.androidtest.ui.home.HomeActivity
import br.com.digio.androidtest.ui.home.viewholders.SpotlightItemViewHolder
import okhttp3.mockwebserver.MockWebServer
import org.junit.Test

class HomeActivityTest {

    private val server = MockWebServer()

    private val context = InstrumentationRegistry.getInstrumentation().targetContext

    @Test
    fun onDisplayTitleShouldHaveHello() {
        launchActivity<HomeActivity>().apply {
            val title = context.getString(R.string.hello_maria)

            moveToState(Lifecycle.State.RESUMED)

            onView(withText(title)).check(matches(isDisplayed()))
        }
    }

    @Test
    fun onDisplayListSpotlightShouldRechargeItem() {
        server.dispatcher = MockServerUtil.dispatcherSuccess
        server.start(MockServerUtil.PORT)

        launchActivity<HomeActivity>().apply {
            onView(withId(R.id.recyMainSpotlight))
                .perform(RecyclerViewActions
                    .actionOnItemAtPosition<SpotlightItemViewHolder>(1, click()))
        }

        server.close()
    }
}