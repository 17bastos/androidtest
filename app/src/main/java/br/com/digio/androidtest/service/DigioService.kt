import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import br.com.digio.androidtest.model.DigioProducts
import br.com.digio.androidtest.service.DigioAPI
import br.com.digio.androidtest.utils.hasNetwork
import okhttp3.Cache
import okhttp3.OkHttpClient
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class DigioService(private val context: Context) {

    private val retrofit : Retrofit by lazy {
        Retrofit.Builder()
            .baseUrl(baseUrl)
            .addConverterFactory(GsonConverterFactory.create())
            .client(okHttpClient)
            .build()
    }

    private val service: DigioAPI by lazy {
        retrofit.create(digioAPI)
    }

    fun getProducts(): Call<DigioProducts> {
        return service.getProducts()
    }

    private val  okHttpClient: OkHttpClient by lazy {
        val cacheSize = (1 * 1024 * 1024).toLong()
        val myCache = Cache(context.cacheDir, cacheSize)

        OkHttpClient.Builder()
            .cache(myCache)
            .addInterceptor { chain ->
                var request = chain.request()
                request = if (hasNetwork(context))
                    request.newBuilder().header("Cache-Control", "public, max-age=" + 5).build()
                else
                    request.newBuilder().header(
                        "Cache-Control",
                        "public, only-if-cached, max-stale=" + 60 * 60 * 24 * 7
                    ).build()
                chain.proceed(request)
            }
            .build()
    }

    companion object {
        private val baseUrl = "https://7hgi9vtkdc.execute-api.sa-east-1.amazonaws.com/"
        private val digioAPI = DigioAPI::class.java
    }
}