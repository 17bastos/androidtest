package br.com.digio.androidtest.ui.home

import DigioService
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.text.SpannableString
import android.text.Spanned
import android.text.style.ForegroundColorSpan
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import br.com.digio.androidtest.ui.home.adapters.ProductAdapter
import br.com.digio.androidtest.R
import br.com.digio.androidtest.ui.home.adapters.SpotlightAdapter
import org.koin.android.ext.android.inject

class HomeActivity : AppCompatActivity(R.layout.activity_main) {

    private val viewModel: HomeViewModel by inject()

    private lateinit var txtMainDigioCash: TextView
    private lateinit var recyMainProducts: RecyclerView
    private lateinit var recyMainSpotlight: RecyclerView
    private lateinit var body: ConstraintLayout
    private lateinit var loadDigioContainer: ConstraintLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initLayout()
        observeViewModel()
        setupDigioCashText()
    }

    override fun onResume() {
        super.onResume()
        viewModel.getProducts()
    }

    private val productAdapter: ProductAdapter by lazy {
        ProductAdapter()
    }

    private val spotlightAdapter: SpotlightAdapter by lazy {
        SpotlightAdapter()
    }

    private fun initLayout() {
        txtMainDigioCash = findViewById(R.id.txtMainDigioCash)
        recyMainProducts = findViewById(R.id.recyMainProducts)
        recyMainSpotlight = findViewById(R.id.recyMainSpotlight)
        body = findViewById(R.id.body)
        loadDigioContainer = findViewById(R.id.loadDigioContainer)

        recyMainProducts.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        recyMainProducts.adapter = productAdapter

        recyMainSpotlight.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        recyMainSpotlight.adapter = spotlightAdapter
    }

    private fun observeViewModel() {
        viewModel.products.observe(this, {
            productAdapter.products = it
        })

        viewModel.spotlights.observe(this, {
            spotlightAdapter.spotlights = it
        })

        viewModel.loadingStatus.observe(this, {
            when (it) {
                LoadingStatusEnum.LOADING -> {
                    body.visibility = View.GONE
                    loadDigioContainer.visibility = View.VISIBLE
                }
                LoadingStatusEnum.SUCCESS -> {
                    loadDigioContainer.visibility = View.GONE
                    body.visibility = View.VISIBLE
                }
                else -> {
                    loadDigioContainer.visibility = View.GONE
                    body.visibility = View.GONE
                    val message = getString(R.string.error)
                    Toast.makeText(this@HomeActivity, message, Toast.LENGTH_SHORT).show()
                }
            }
        })
    }

    private fun setupDigioCashText() {
        val digioCacheText = getString(R.string.digio_cache)
        txtMainDigioCash.text = SpannableString(digioCacheText).apply {
            setSpan(
                ForegroundColorSpan(
                    ContextCompat.getColor(this@HomeActivity, R.color.blue_darker)
                ),
                0,
                5,
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
            )
            setSpan(
                ForegroundColorSpan(
                    ContextCompat.getColor(this@HomeActivity, R.color.font_color_digio_cash)
                ),
                6,
                digioCacheText.length,
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
            )
        }
    }
}