package br.com.digio.androidtest.ui.home

import DigioService
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import br.com.digio.androidtest.model.DigioProducts
import br.com.digio.androidtest.model.Product
import br.com.digio.androidtest.model.Spotlight
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class HomeViewModel(private val digioService: DigioService) : ViewModel() {
    val products: MutableLiveData<List<Product>> = MutableLiveData()
    val spotlights: MutableLiveData<List<Spotlight>> = MutableLiveData()
    val loadingStatus: MutableLiveData<LoadingStatusEnum> = MutableLiveData(LoadingStatusEnum.LOADING)

    fun getProducts() {
        loadingStatus.value = LoadingStatusEnum.LOADING
        return digioService.getProducts()
            .enqueue(object : Callback<DigioProducts> {
                override fun onResponse(call: Call<DigioProducts>, response: Response<DigioProducts>) {
                    products.value = response.body()?.products ?: listOf()
                    spotlights.value = response.body()?.spotlight ?: listOf()
                    loadingStatus.value = LoadingStatusEnum.SUCCESS
                }

                override fun onFailure(call: Call<DigioProducts>, t: Throwable) {
                    loadingStatus.value = LoadingStatusEnum.ERROR
                }
            })
    }
}

enum class LoadingStatusEnum {
    LOADING, SUCCESS, ERROR
}