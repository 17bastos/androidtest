package br.com.digio.androidtest.service

import br.com.digio.androidtest.model.DigioProducts
import retrofit2.Call
import retrofit2.http.GET

interface DigioAPI {
    @GET("sandbox/products")
    fun getProducts(): Call<DigioProducts>
}