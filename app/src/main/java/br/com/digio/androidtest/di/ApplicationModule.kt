import br.com.digio.androidtest.ui.home.HomeViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val applicationModule = module {
    single { DigioService(get()) }
    viewModel { HomeViewModel(get()) }
}