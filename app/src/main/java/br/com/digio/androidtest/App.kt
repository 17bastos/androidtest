package br.com.digio.androidtest

import android.app.Application
import applicationModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class App: Application() {
    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidContext(this@App)
            modules(
                applicationModule
            )
        }
    }
}