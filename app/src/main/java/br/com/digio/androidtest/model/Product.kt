package br.com.digio.androidtest.model

data class Product(
    val imageURL: String,
    val name: String,
    val description: String
)